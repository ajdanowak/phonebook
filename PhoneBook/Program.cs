﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace PhoneBook
{
    class Program
    {

        static void Main(string[] args)
        {
            var exit = false;
            PhoneBook userPhoneBook = new PhoneBook();

            while (exit == false)
            {
                Console.WriteLine("Write 'add' if you want to add new contact\n" +
                                  "Write 'find' to find the contact\n" +
                                  "Write 'delete' if you want to remove contact\n" +
                                  "Write 'exit' to close the application\n");

                var command = Console.ReadLine();

                if (command == "add")
                {
                    Console.WriteLine("Write contact name");
                    var name = Console.ReadLine();
                    Console.WriteLine("Write contact surname");
                    var surname = Console.ReadLine();
                    Console.WriteLine("Write the number");
                    var number = Console.ReadLine();
                    var contact = new Contact
                    {
                        Name = name,
                        Number = number,
                        Surname = surname
                    };
                    userPhoneBook.AddContact(contact);
                    Console.WriteLine("Contact was added");
                }
                else if (command == "find")
                {
                    Console.WriteLine("Write searching name or surname");
                    var phrase = Console.ReadLine();
                    var resultList = userPhoneBook.FindContacts(phrase);
                    foreach (Contact contact in resultList)
                    {
                        Console.WriteLine(contact.GetFullInfo());
                    }
                }
                else if (command == "delete")
                {
                    Console.WriteLine("Write name or surname of contact you want to remove");
                    var phrase = Console.ReadLine();
                    var resultList = userPhoneBook.FindContacts(phrase);
                    foreach (Contact contact in resultList)
                    {
                        Console.WriteLine("Type 'yes' if you want to remove this contact:");
                        Console.WriteLine(contact.GetFullInfo());
                        var confirmation = Console.ReadLine();
                        if (confirmation == "yes")
                        {
                            userPhoneBook.DeleteContact(contact);
                            Console.WriteLine("Contact was deleted");
                        }
                    }
                }
                else if (command == "exit")
                {
                    exit = true;
                }
                else
                {
                    Console.WriteLine("You typed in unsupported command. Try again");
                }
            }

        }
    }
}