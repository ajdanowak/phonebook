﻿namespace PhoneBook
{
    public class Contact
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Number { get; set; }

        public string GetFullInfo()
        {
            return Name + " " + Surname + " " + Number;
        }

    }
}