﻿using System;
using System.Collections.Generic;

namespace PhoneBook
{
    public class PhoneBook
    {
        private readonly List<Contact> _contactList;
        public PhoneBook()
        {
            _contactList = new List<Contact>();
        }

        public void AddContact(Contact contact)
        {
            _contactList.Add(contact);
        }

        public List<Contact> FindContacts(string phrase)
        {
            return _contactList.FindAll(x => x.Name.Contains(phrase) || x.Surname.Contains(phrase));
        }

        public void DeleteContact(Contact contact)
        {
            _contactList.Remove(contact);
        }
    }
}
